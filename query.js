function sendQuery(callback, name, con) {
    con.query('SELECT * FROM location WHERE Location_name = "' + name + '";', function(err, result) {
        if (err) throw err;
        callback(result);
    });
}

function getDateEventsQuery(callback, start, end, con) {
    con.query('SELECT * FROM event_date INNER JOIN event ' +
        'ON event_date.Event_id = event.Event_id WHERE Date BETWEEN "' + start + '" AND "' + end + '";', function(err, result) {
        if (err) throw err;
        //result.numOfRows = result.length;
        callback(result);
    });
}

function insertEventQuery(callback, name, type, location_id, con) {
    con.query('INSERT INTO Event (Name, Type, Location_Location_id)' +
        ' VALUES ("' + name + '", "' + type + '", ' + location_id + ');', function(err, result) {
        if (err) throw err;
        callback(result);
    });
}

function insertDateQuery(callback, date, eventId, con) {
    con.query('INSERT INTO Event_date (Date, Event_id)' +
        ' VALUES ("' + date + '", ' + eventId + ');', function(err, result) {
        if (err) throw err;
        callback(result);
    });
}

module.exports = Object.assign({sendQuery, getDateEventsQuery, insertEventQuery, insertDateQuery});