var express = require('express');
var cors = require('cors');
var bodyParser = require('body-parser');
var queries = require('./query.js');
var app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var mysql = require('mysql');
var con = mysql.createConnection({
    host: "localhost",
    user: "event_db_user",
    password: "password",
    database: "event_db"
});

con.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
});

app.get('/api', function (req, res) {
    let name = req.query.name;
    console.log('fetching data ...');
    console.log('param is ' + name);
    queries.sendQuery(function(data) {
        res.send(data);
    }, name, con);
});

app.get('/api/events', function (req, res) {
    let start = req.query.start;
    let end = req.query.end;
    console.log(start + ' ' + end);
    queries.getDateEventsQuery(function(data) {
        res.send(data);
    }, start, end, con);
});

app.post('/api/event', function(req, res) {
    console.log('receiving data ...');
    console.log('body is ', req.body);
    let name = req.body.Name;
    let type = req.body.Type;
    let location = req.body.Location_Location_id;
    let date = req.body.Event_date.Date;
    queries.insertEventQuery(function(data) {
        console.log('id:', data.insertId);
        queries.insertDateQuery(function(data2) {
            res.send("Succesfully inserted!");
        }, date, data.insertId, con);
    }, name, type, location, con);
});

var server = app.listen(8081, function () {
    var host = server.address().address;
    var port = server.address().port;

    console.log("Example app listening at http://%s:%s", host, port);
});